package exceptions;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.file.FileAlreadyExistsException;

/**
 * Created by kim on 4/3/16.
 */
public class VerbuggteKlasse {
    public String aktuellesJahr() throws IOException {
        System.out.println("1");
        System.out.println("2");
        System.out.println("3");
        try {
            rufeZeitVonServerAb();
        } finally {
            System.out.println("HELLO WORLD");
        }
        System.out.println("4");
        System.out.println("5");
        return "Zweitausendf?nfzaehn";
    }

    public void rufeZeitLokalAb() {
        //...
    }

    public void rufeZeitVonServerAb() throws IOException {
        Socket s = new Socket();
        s.connect(new InetSocketAddress("127.0.0.1", 54));
        //....
    }
}
