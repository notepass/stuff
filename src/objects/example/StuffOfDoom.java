package objects.example;

import objects.BaseClass;
import objects.ExtendedClass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kim on 4/3/16.
 */
public class StuffOfDoom {
    public static void main(String[] args) {

        BaseClass bc = new ExtendedClass();
        System.out.println(bc.stuff());

    }
}
