package objects;

/**
 * Created by kim on 4/3/16.
 */
public class ExtendedClass extends BaseClass {
    public static void stuffExtended() {

    }

    public boolean abcExtended() {
        return true;
    }

    public String stuff() {
        return "ExtendedClass";
    }
}
