import java.net.Inet4Address;
import java.net.Socket;

/**
 * Created by kim on 3/5/16.
 */
public class Portscanner {
    static int numberOfThreads = 6;

    public static void main(String[] args) {
        for (int i=1;i<=numberOfThreads;i++) {
            final int finalI = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int threadId = finalI;

                    for (int j=threadId;j<=65535;j+=numberOfThreads) {
                        try {
                            Socket s = new Socket("192.168.19.1", j);
                            s.getInputStream();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }
    }
}
