import java.util.Scanner;

/**
 * Created by kim on 11/2/15.
 */
public class Stuff {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("n: ");
            String n = scanner.nextLine();
            if (n.isEmpty()) {
                break;
            }
            System.out.print("m: ");
            String m = scanner.nextLine();
            if (m.isEmpty()) {
                break;
            }
            try {
                System.out.println("ggt: " + ggt(Integer.valueOf(n), Integer.valueOf(m)));
            } catch (Exception e) {
                System.err.println("Fehlerhafte Zahl");
                continue;
            }
        }
    }

    public static int ggt(int m, int n) {
        while (m != 0) {
            int tmp = m;
            m = n % m;
            n = tmp;
        }
        return n;
    }
}
