package moreStuff;

import moreStuff.InterfaceDings;

/**
 * Created by kim on 4/3/16.
 */
public class ImplementedInterfaceDings implements InterfaceDings, InterfaceDingsV2 {
    @Override
    public void stub1() {
        System.out.println("Stub 1");
    }

    @Override
    public void stub2() {
        System.out.println("Stub 2");
    }

    @Override
    public void stub3() {
        System.out.println("Stub 3");
    }

    @Override
    public void stub4() {
        System.out.println("Stub 4");
    }

    @Override
    public void stub5() {
        System.out.println("Stub 5");
    }

    @Override
    public void stub6() {
        System.out.println("Stub 6");
    }

    @Override
    public void stub7() {
        System.out.println("Stub 7");
    }
}
