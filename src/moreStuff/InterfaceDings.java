package moreStuff;

/**
 * Created by kim on 4/3/16.
 */
public interface InterfaceDings {
    public void stub1();
    public void stub2();
    public void stub3();
    public void stub4();

    /**
     * public     -> public
     * protected  -> private
     * private    -> /
     */
}
