package moreStuff.wathever;

import moreStuff.*;

/**
 * Created by kim on 4/3/16.
 */
public class Tester {
    public static void main(String[] args) {
        InterfaceDings i = new ImplementedInterfaceDings();
        InterfaceDingsV2 i2 = new ImplementedInterfaceDings();
        ImplementedInterfaceDings i3 = new ImplementedInterfaceDings();
        x(i);
        x(i3);

        y(i2);
        y(i3);

        z(i3);
    }

    public static void x(InterfaceDings a) {
        a.stub1();
    }

    public static void y(InterfaceDingsV2 a) {
        a.stub5();
    }

    public static void z(ImplementedInterfaceDings a) {
        a.stub1();
        a.stub7();
    }

}
